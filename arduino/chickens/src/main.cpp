// Chicken door manager for FarmNet.  
// Author: Gabriel Dulac-Arnold <gabe@squirrelsoup.net>
#include <RH_RF95.h>

#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>

#define RFM95_CS 8
#define RFM95_RST 4
#define RFM95_INT 3

#define BOARD_ADDR 5
#define MASTER_ADDR 1
#define SENSOR_AN_PIN 0


// Change to 434.0 or other frequency, must match RX's freq!
// #define RF95_FREQ 868.1
#define RF95_FREQ 868.1
// #define DATA_PIN A0

// Timeout on door motor (in 0.1s)
#define DOOR_TIMEOUT 300 // 60s

// Sensor pins
#define LIGHT_SENSOR 0
#define BAT_SENSOR 1
#define BAT_VOLTAGE_MULT 0.0344
// Limit Switches
#define LIMIT_OPEN 10
#define LIMIT_CLOSED 9
// Motor controls
#define DOOR_OPEN 11
#define DOOR_CLOSE 12
#define DHT_PIN 6
// Luminosity limits
#define LUM_CLOSE 30
#define LUM_OPEN 500
// Push & poll frequencies
#define PUSH_FREQ 60 * 60 * 1000 //1h
#define POLL_FREQ 1 * 30 * 1000 //30s
// #define PUSH_FREQ 1 * 15 * 1000 //1h
// #define POLL_FREQ 1 * 10 * 1000 //5m

enum class Door {open, closed, unknown};


char message_buf[80];

// Singleton instance of the radio driver
RH_RF95 rf95(RFM95_CS, RFM95_INT);
DHT dht(DHT_PIN, DHT22);

int16_t packetnum = 0;  // packet counter, we increment per xmission
uint8_t r_size = 0;
float temp = 0;
float humidity = 0;
int luminosity = 0;

unsigned long push_timestamp = 0;
unsigned long poll_timestamp = 0;
Door door_status = Door::unknown;
Door last_door_status = Door::unknown;
uint8_t error = 0;
String str_error = "NONE";

void sendRadio(char * message_buf, uint8_t r_size) {
  Serial.print("Sending...");
  rf95.setHeaderId(packetnum++);
  Serial.println((char*)message_buf);
  delay(10);
  rf95.send((uint8_t *)message_buf, r_size);
  Serial.println("Waiting for packet to complete..."); delay(10);
  rf95.waitPacketSent();
}

void sendTHLV() {
  float temp = dht.readTemperature(false);
  float humidity = dht.readHumidity();
  int illumination = analogRead(LIGHT_SENSOR);
  float bat_voltage = analogRead(BAT_SENSOR) * BAT_VOLTAGE_MULT;
  String str_temp = String(temp);
  String str_humidity = String(humidity);
  String str_illumination = String(illumination);
  String str_bat_voltage = String(bat_voltage);
  r_size = sprintf(message_buf, "T=%s;H=%s;L=%s;V=%s;E=%s;",
                   str_temp.c_str(), str_humidity.c_str(),
                   str_illumination.c_str(), str_bat_voltage.c_str(),
                   str_error.c_str());
  sendRadio(message_buf, r_size);
}

void sendError(String error_str) {
  r_size = sprintf((char*)message_buf, "E=%s;", error_str.c_str());
  sendRadio(message_buf, r_size);
}

void sendDebug(String error_str) {
  r_size = sprintf((char*)message_buf, "G=%s;", error_str.c_str());
  sendRadio(message_buf, r_size);
}

void sendDoorStatus() {
  String status_str;
  if (door_status == Door::open) {
    status_str = String("OPEN");
  }
  if (door_status == Door::closed) {
    status_str = String("CLOSED");
  }
  if (door_status == Door::unknown) {
    status_str = String("UNKNOWN");
  }
  r_size = sprintf((char*)message_buf, "D=%s;", status_str.c_str());
  sendRadio(message_buf, r_size);
}

// Get the actual hardware status of the door according to the limit switches
Door getDoorStatus() {
  // Serial.println(digitalRead(LIMIT_OPEN));
  // Serial.println(digitalRead(LIMIT_CLOSED));
  if (digitalRead(LIMIT_OPEN) == LOW)
    return Door::open;
  if (digitalRead(LIMIT_CLOSED) == LOW)
    return Door::closed;
  return Door::unknown;
}

// Set the door to 'state'.  Make sure that if the door never hits the appopriate
// limit switch,  that we don't hang indefinitely and send out an error by radio.
void moveDoor(Door goal_state) {
  uint8_t door_pin;
  uint16_t counter = 0;
  if (goal_state == door_status) {
    return;
  }
  // This should not happen
  if (goal_state == Door::unknown) {
    str_error = String("MOVE_UNK");
    error = 1;
    return;
  }
  if (goal_state == Door::open) {
    door_pin = DOOR_OPEN;
  }
  if (goal_state == Door::closed) {
    door_pin = DOOR_CLOSE;
  }

  digitalWrite(door_pin, HIGH);
  while (getDoorStatus() != goal_state) {
    Serial.println(counter);
    delay(100);
    if (counter++ > DOOR_TIMEOUT) break;
  }
  delay(250);
  digitalWrite(door_pin, LOW);
  last_door_status = door_status;
  door_status = getDoorStatus();
}

void manageDoor() {
  Door direction = Door::unknown;
  if (error == 1) {
    Serial.println("ERROR"  + str_error);
    return;
  }

  int illumination = analogRead(LIGHT_SENSOR);
  if (door_status == Door::open && illumination < LUM_CLOSE) {
    direction = Door::closed;
    Serial.println("Closing");
  } else if (door_status == Door::closed && illumination > LUM_OPEN) {
    direction = Door::open;
    Serial.println("Opening");
  } else {
    return;
  }
  if (direction == Door::unknown) {
    if (last_door_status != Door::unknown) {
      direction = last_door_status;
      sendError("DOOR_RESET");
    } else {
      str_error = "DOOR_STUCK_UNKNOW3";
      error = 1;
      return;
    }
  }

  // Attempt a door move
  moveDoor(direction);
  // Check if door is stuck (didn't even leave starting position)
  // In this case it is best to have a human intervention
  if (door_status == last_door_status) {
    str_error = String(door_status == Door::open ? "DOOR_STUCK_OPEN" : "DOOR_STUCK_CLOSED");
    Serial.println(str_error);
    // Set the error flag to avoid any more potentially dangerous actions
    error = 1;
    return;
  }

  // If the door is stuck in between, try and re-do the action one time
  if (door_status == Door::unknown) {
    Serial.println(
      "Door in uncertain state, bringing back to last state and trying again.");
    moveDoor(last_door_status);
    // If reversing the door didn't un-stick it, go into error mode.
    if (door_status == Door::unknown) {
      str_error = String("DOOR_STUCK_UNKNOW1");
      error = 1;
      return;
    } else {
      moveDoor(direction);  // Let's try this again
    }
    // If it got stuck someplace in between again, go into error mode.
    if (door_status == Door::unknown) {
      str_error = String("DOOR_STUCK_UNKNOW2");
      error = 1;
      return;
    }
  }
  sendDoorStatus();
  sendDebug("DOOR_MOVED");
}

void setup()
{
  pinMode(RFM95_RST, OUTPUT);
  digitalWrite(RFM95_RST, HIGH);

  // while (!Serial);
  Serial.begin(9600);
  delay(100);

  Serial.println("Feather LoRa TX Test!");

  // manual reset
  digitalWrite(RFM95_RST, LOW);
  delay(10);
  digitalWrite(RFM95_RST, HIGH);
  delay(10);

  while (!rf95.init()) {
    Serial.println("LoRa radio init failed");
    while (1);
  }
  Serial.println("LoRa radio init OK!");

  // Defaults after init are 434.0MHz, modulation GFSK_Rb250Fd250, +13dbM
  if (!rf95.setFrequency(RF95_FREQ)) {
    Serial.println("setFrequency failed");
    while (1);
  }
  Serial.print("Set Freq to: "); Serial.println(RF95_FREQ);

  // Defaults after init are 868.1MHz, 13dBm, Bw = 125 kHz, Cr = 4/5, Sf = 128chips/symbol, CRC on

  // The default transmitter power is 13dBm, using PA_BOOST.
  // If you are using RFM95/96/97/98 modules which uses the PA_BOOST transmitter pin, then
  // you can set transmitter powers from 5 to 23 dBm:
  rf95.setTxPower(23, false);

  rf95.setThisAddress(BOARD_ADDR);
  rf95.setHeaderTo(MASTER_ADDR);
  rf95.setHeaderFrom(BOARD_ADDR);

  dht.begin();

  pinMode(LIMIT_OPEN, INPUT_PULLUP);
  pinMode(LIMIT_CLOSED, INPUT_PULLUP);
  digitalWrite(DOOR_OPEN, LOW);
  digitalWrite(DOOR_CLOSE, LOW);

  delay(100);
  sendTHLV();

  door_status = getDoorStatus();
  last_door_status = door_status;
  sendDoorStatus();
  manageDoor();
}

void loop() {
  if (millis() - push_timestamp > PUSH_FREQ) {
  // Every hour send T&H + Door
    sendTHLV();
    sendDoorStatus();
    push_timestamp = millis();
  }

  if (millis() - poll_timestamp > POLL_FREQ) {
    // door_status = getDoorStatus();
    // If the door status changed on its own, send the door back to where it was
    // if (door_status != last_door_status && error != 1) {
    //   sendDoorStatus();
    //   moveDoor(last_door_status);
    //   sendError("DOOR_RESET");
    // }
    // last_door_status = door_status;
    manageDoor();

    poll_timestamp = millis();
  }
  // Serial.println("time");
  // Serial.println(push_timestamp);
  // Serial.println(poll_timestamp);
}
