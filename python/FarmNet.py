#!/usr/bin/env python
import logging
import re
import time
import sqlite3
import argparse

from rf95 import RF95
from IrrigationController import IrrigationController

MASTER_ADDR = 1


class TimeSeriesStore(object):

    def __init__(self, db_file=None):
        self.db_file = db_file
        logging.info("Logging data to {}.".format(db_file))
        self.conn = sqlite3.connect(db_file)
        self.c = self.conn.cursor()
        self._init_db()

    def _init_db(self):
        """Create the database if needed."""
        self.c.execute("""CREATE TABLE IF NOT EXISTS events
            (Timestamp DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
             Key TEXT NOT NULL,
             Value INTEGER NOT NULL,
             PRIMARY KEY (Timestamp, Key));""")

    def add_event(self, key, value):
        ts = int(time.time())
        logging.info("Storing: ts={}, key={}, value={}".format(ts, key, value))

        try:
            self.c.execute("INSERT INTO events VALUES (?, ?, ?)",
                           (ts, key, value))
            self.conn.commit()

        except sqlite3.IntegrityError:
            logging.error(
                "Tried to insert non-unique event : "
                "ts={}, key={}, value={} ".format(ts, key, value))

        except Exception, e:
            logging.error("Caught exception on event insertion: {}".format(e))

    def __del__(self):
        self.close()

    def close(self):
        self.conn.close()


def send_update(rf95, to_addr, telemetry_string):
    telem_data = rf95.str_to_data(telemetry_string)
    logging.debug(len(telem_data))
    header = [to_addr, MASTER_ADDR, len(telem_data), 0]
    rf95.send(header + telem_data)
    rf95.wait_packet_sent()
    logging.info("Sending Update")
    pass


def init_radio(cs=0, int_pin=4, reset_pin=17, freq=868.10, tx_power=13):
    """Init the radio with sensible defaults and return the radio object."""
    rf95 = RF95(cs=cs, int_pin=int_pin, reset_pin=reset_pin)

    if not rf95.init():
        logging.warn("RF95 not found")
        rf95.cleanup()
        quit(1)

    else:
        logging.info("RF95 LoRa mode ok")

    # set frequency and power
    rf95.set_frequency(freq)
    rf95.set_tx_power(tx_power)
    return rf95


def main(db_file):
    rf95 = init_radio()
    send_ts = time.time()
    tx_interval = 10
    tss = TimeSeriesStore(db_file)
    ic = IrrigationController()

    while True:
        try:
            # Send out any data we need to send every tx_interval seconds
            if False and time.time() - send_ts > tx_interval:
                telem_data = ic.get_telem()
                send_update(rf95, to_addr=2, telemetry_string=telem_data)
                send_ts = time.time()

            if not rf95.available():
                continue

            data = rf95.recv()
            # This is the header order for the radiohead library
            tx_to, tx_from, tx_len, flags = data[0:4]
            msg_buf = data[4:]
            msg_str = ''.join(map(chr, msg_buf))
            logging.debug("Received message: {}".format(msg_str))
            # All sensors should send data as FOO=1;BAR=2;....
            values = re.findall('(.+?)=(.+?);', msg_str)
            for sensor_key, val in values:
                # Key format is client_id-sensor_id
                key = "{}-{}".format(tx_from, sensor_key)
                tss.add_event(key, val)

        except KeyboardInterrupt:
            logging.info("Quitting...")
            rf95.set_mode_idle()
            rf95.cleanup()
            tss.close()
            quit(0)
        except ValueError, e:
            logging.error('Something happened while reading: {}.'.format(e))
            continue


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--db_file', help='sqlite3 file',
                        default='farmnet.sqlite3')
    parser.add_argument('--log', default='WARNING')
    args = parser.parse_args()

    numeric_level = getattr(logging, args.log.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: %s' % args.log)

    try:
        import coloredlogs
        coloredlogs.install(level=numeric_level)
    except ImportError:
        logging.basicConfig(level=numeric_level)

    main(args.db_file)
